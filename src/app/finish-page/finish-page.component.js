"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var FinishPageComponent = /** @class */ (function () {
    function FinishPageComponent() {
    }
    FinishPageComponent.prototype.ngOnInit = function () {
    };
    FinishPageComponent = __decorate([
        core_1.Component({
            selector: 'ns-finish-page',
            templateUrl: './finish-page.component.html',
            styleUrls: ['./finish-page.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [])
    ], FinishPageComponent);
    return FinishPageComponent;
}());
exports.FinishPageComponent = FinishPageComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmluaXNoLXBhZ2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZmluaXNoLXBhZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBUWxEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQixzQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUxVLG1CQUFtQjtRQU4vQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixXQUFXLEVBQUUsOEJBQThCO1lBQzNDLFNBQVMsRUFBRSxDQUFDLDZCQUE2QixDQUFDO1lBQzFDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDOztPQUNXLG1CQUFtQixDQU8vQjtJQUFELDBCQUFDO0NBQUEsQUFQRCxJQU9DO0FBUFksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtZmluaXNoLXBhZ2UnLFxuICB0ZW1wbGF0ZVVybDogJy4vZmluaXNoLXBhZ2UuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9maW5pc2gtcGFnZS5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIEZpbmlzaFBhZ2VDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIl19