import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-finish-page',
  templateUrl: './finish-page.component.html',
  styleUrls: ['./finish-page.component.css'],
  moduleId: module.id,
})
export class FinishPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
