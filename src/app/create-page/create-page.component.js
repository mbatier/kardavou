"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CreatePageComponent = /** @class */ (function () {
    function CreatePageComponent() {
        this.design = true;
        this.choice = 'design';
        this.tabDesign = [
            { name: 'Design 1', value: 'layout1' },
            { name: 'Design 2', value: 'layout2' },
            { name: 'Design 3', value: 'layout3' },
            { name: 'Design 4', value: 'layout4' },
            { name: 'Design 5', value: 'layout5' },
            { name: 'Design 6', value: 'layout6' }
        ];
        this.tabTypo = [
            { name: 'Typo 1', value: 'layout1' },
            { name: 'Typo 2', value: 'layout2' },
            { name: 'Typo 3', value: 'layout3' },
            { name: 'Typo 4', value: 'layout4' },
            { name: 'Typo 5', value: 'layout5' },
            { name: 'Typo 6', value: 'layout6' }
        ];
        this.tabColor = [
            { name: 'Color 1', value: 'layout1' },
            { name: 'Color 2', value: 'layout2' },
            { name: 'Color 3', value: 'layout3' },
            { name: 'Color 4', value: 'layout4' },
            { name: 'Color 5', value: 'layout5' },
            { name: 'Color 6', value: 'layout6' }
        ];
    }
    CreatePageComponent.prototype.ngOnInit = function () {
    };
    CreatePageComponent.prototype.onTap = function () {
        console.log("hello");
    };
    CreatePageComponent.prototype.tapChoice = function (value) {
        this.choice = value;
    };
    CreatePageComponent = __decorate([
        core_1.Component({
            selector: 'ns-create-page',
            templateUrl: './create-page.component.html',
            styleUrls: ['./create-page.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [])
    ], CreatePageComponent);
    return CreatePageComponent;
}());
exports.CreatePageComponent = CreatePageComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlLXBhZ2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY3JlYXRlLXBhZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBUWxEO0lBK0JFO1FBOUJPLFdBQU0sR0FBWSxJQUFJLENBQUM7UUFDOUIsV0FBTSxHQUFHLFFBQVEsQ0FBQTtRQUVqQixjQUFTLEdBQUc7WUFDVixFQUFDLElBQUksRUFBQyxVQUFVLEVBQUUsS0FBSyxFQUFDLFNBQVMsRUFBQztZQUNsQyxFQUFDLElBQUksRUFBQyxVQUFVLEVBQUUsS0FBSyxFQUFDLFNBQVMsRUFBQztZQUNsQyxFQUFDLElBQUksRUFBQyxVQUFVLEVBQUUsS0FBSyxFQUFDLFNBQVMsRUFBQztZQUNsQyxFQUFDLElBQUksRUFBQyxVQUFVLEVBQUUsS0FBSyxFQUFDLFNBQVMsRUFBQztZQUNsQyxFQUFDLElBQUksRUFBQyxVQUFVLEVBQUUsS0FBSyxFQUFDLFNBQVMsRUFBQztZQUNsQyxFQUFDLElBQUksRUFBQyxVQUFVLEVBQUUsS0FBSyxFQUFDLFNBQVMsRUFBQztTQUNuQyxDQUFBO1FBRUQsWUFBTyxHQUFHO1lBQ1IsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxTQUFTLEVBQUM7WUFDaEMsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxTQUFTLEVBQUM7WUFDaEMsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxTQUFTLEVBQUM7WUFDaEMsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxTQUFTLEVBQUM7WUFDaEMsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxTQUFTLEVBQUM7WUFDaEMsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxTQUFTLEVBQUM7U0FDakMsQ0FBQTtRQUVELGFBQVEsR0FBRztZQUNULEVBQUMsSUFBSSxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsU0FBUyxFQUFDO1lBQ2pDLEVBQUMsSUFBSSxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsU0FBUyxFQUFDO1lBQ2pDLEVBQUMsSUFBSSxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsU0FBUyxFQUFDO1lBQ2pDLEVBQUMsSUFBSSxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsU0FBUyxFQUFDO1lBQ2pDLEVBQUMsSUFBSSxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsU0FBUyxFQUFDO1lBQ2pDLEVBQUMsSUFBSSxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsU0FBUyxFQUFDO1NBQ2xDLENBQUE7SUFFZSxDQUFDO0lBRWpCLHNDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsbUNBQUssR0FBTDtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFdkIsQ0FBQztJQUNELHVDQUFTLEdBQVQsVUFBVSxLQUFLO1FBQ2IsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUE7SUFDckIsQ0FBQztJQTFDVSxtQkFBbUI7UUFOL0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsV0FBVyxFQUFFLDhCQUE4QjtZQUMzQyxTQUFTLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQztZQUMxQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDcEIsQ0FBQzs7T0FDVyxtQkFBbUIsQ0E0Qy9CO0lBQUQsMEJBQUM7Q0FBQSxBQTVDRCxJQTRDQztBQTVDWSxrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICducy1jcmVhdGUtcGFnZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9jcmVhdGUtcGFnZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NyZWF0ZS1wYWdlLmNvbXBvbmVudC5jc3MnXSxcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbn0pXG5leHBvcnQgY2xhc3MgQ3JlYXRlUGFnZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIHB1YmxpYyBkZXNpZ246IGJvb2xlYW4gPSB0cnVlO1xuICBjaG9pY2UgPSAnZGVzaWduJ1xuXG4gIHRhYkRlc2lnbiA9IFtcbiAgICB7bmFtZTonRGVzaWduIDEnLCB2YWx1ZTonbGF5b3V0MSd9LFxuICAgIHtuYW1lOidEZXNpZ24gMicsIHZhbHVlOidsYXlvdXQyJ30sXG4gICAge25hbWU6J0Rlc2lnbiAzJywgdmFsdWU6J2xheW91dDMnfSxcbiAgICB7bmFtZTonRGVzaWduIDQnLCB2YWx1ZTonbGF5b3V0NCd9LFxuICAgIHtuYW1lOidEZXNpZ24gNScsIHZhbHVlOidsYXlvdXQ1J30sXG4gICAge25hbWU6J0Rlc2lnbiA2JywgdmFsdWU6J2xheW91dDYnfVxuICBdXG5cbiAgdGFiVHlwbyA9IFtcbiAgICB7bmFtZTonVHlwbyAxJywgdmFsdWU6J2xheW91dDEnfSxcbiAgICB7bmFtZTonVHlwbyAyJywgdmFsdWU6J2xheW91dDInfSxcbiAgICB7bmFtZTonVHlwbyAzJywgdmFsdWU6J2xheW91dDMnfSxcbiAgICB7bmFtZTonVHlwbyA0JywgdmFsdWU6J2xheW91dDQnfSxcbiAgICB7bmFtZTonVHlwbyA1JywgdmFsdWU6J2xheW91dDUnfSxcbiAgICB7bmFtZTonVHlwbyA2JywgdmFsdWU6J2xheW91dDYnfVxuICBdXG5cbiAgdGFiQ29sb3IgPSBbXG4gICAge25hbWU6J0NvbG9yIDEnLCB2YWx1ZTonbGF5b3V0MSd9LFxuICAgIHtuYW1lOidDb2xvciAyJywgdmFsdWU6J2xheW91dDInfSxcbiAgICB7bmFtZTonQ29sb3IgMycsIHZhbHVlOidsYXlvdXQzJ30sXG4gICAge25hbWU6J0NvbG9yIDQnLCB2YWx1ZTonbGF5b3V0NCd9LFxuICAgIHtuYW1lOidDb2xvciA1JywgdmFsdWU6J2xheW91dDUnfSxcbiAgICB7bmFtZTonQ29sb3IgNicsIHZhbHVlOidsYXlvdXQ2J31cbiAgXVxuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBvblRhcCgpe1xuICAgIGNvbnNvbGUubG9nKFwiaGVsbG9cIik7XG4gICAgXG4gIH1cbiAgdGFwQ2hvaWNlKHZhbHVlKSB7XG4gICAgdGhpcy5jaG9pY2UgPSB2YWx1ZVxuICB9XG5cbn1cbiJdfQ==