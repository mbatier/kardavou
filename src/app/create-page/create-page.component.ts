import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.css'],
  moduleId: module.id,
})
export class CreatePageComponent implements OnInit {
  public design: boolean = true;
  choice = 'design'

  tabDesign = [
    {name:'Design 1', value:'layout1'},
    {name:'Design 2', value:'layout2'},
    {name:'Design 3', value:'layout3'},
    {name:'Design 4', value:'layout4'},
    {name:'Design 5', value:'layout5'},
    {name:'Design 6', value:'layout6'}
  ]

  tabTypo = [
    {name:'Typo 1', value:'layout1'},
    {name:'Typo 2', value:'layout2'},
    {name:'Typo 3', value:'layout3'},
    {name:'Typo 4', value:'layout4'},
    {name:'Typo 5', value:'layout5'},
    {name:'Typo 6', value:'layout6'}
  ]

  tabColor = [
    {name:'Color 1', value:'layout1'},
    {name:'Color 2', value:'layout2'},
    {name:'Color 3', value:'layout3'},
    {name:'Color 4', value:'layout4'},
    {name:'Color 5', value:'layout5'},
    {name:'Color 6', value:'layout6'}
  ]

  constructor() { }

  ngOnInit() {
  }

  onTap(){
    console.log("hello");
    
  }
  tapChoice(value) {
    this.choice = value
  }

}
